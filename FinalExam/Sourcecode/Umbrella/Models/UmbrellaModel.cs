using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Umbrella.Models
{
    public class User : IdentityUser{
    public string FirstName { get; set;}
    public string LastName { get; set;}
    }

    public class Medic {
        public int MedicID { get; set; }
        public string MedicName { get; set; }
        public string Pro { get; set;}
        public int Stork { get; set; }
        public string  imageFileName {get; set;}

        public string UserId {get; set;}
        public User MedicUser {get; set;}
    }
}
