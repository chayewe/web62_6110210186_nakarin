using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Umbrella.Data;
using Umbrella.Models;

using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace Umbrella.Pages.Newmedic
{
    public class EditModel : PageModel
    {
        private readonly Umbrella.Data.UmbrellaContext _context;

        private IHostingEnvironment _environment;

        public EditModel(Umbrella.Data.UmbrellaContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        [BindProperty]
        public Medic Medic { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Medic = await _context.Newmedic.FirstOrDefaultAsync(m => m.MedicID == id);

            if (Medic == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Medic).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicExists(Medic.MedicID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            IFormFile ImageFile = Request.Form.Files[0];

            var file = Path.Combine(_environment.ContentRootPath, "wwwroot/images/img", ImageFile.FileName);
            using (var fileStream = new FileStream(file, FileMode.Create))
            {
                await ImageFile.CopyToAsync(fileStream);
            }
            
            Medic.imageFileName =  Request.Form.Files[0].FileName;

             _context.Newmedic.Attach(Medic);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }

        private bool MedicExists(int id)
        {
            return _context.Newmedic.Any(e => e.MedicID == id);
        }
    }
}
