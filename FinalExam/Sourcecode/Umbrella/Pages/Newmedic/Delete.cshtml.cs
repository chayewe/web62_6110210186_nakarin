using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Umbrella.Data;
using Umbrella.Models;

namespace Umbrella.Pages.Newmedic
{
    public class DeleteModel : PageModel
    {
        private readonly Umbrella.Data.UmbrellaContext _context;

        public DeleteModel(Umbrella.Data.UmbrellaContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Medic Medic { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Medic = await _context.Newmedic.FirstOrDefaultAsync(m => m.MedicID == id);

            if (Medic == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Medic = await _context.Newmedic.FindAsync(id);

            if (Medic != null)
            {
                _context.Newmedic.Remove(Medic);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
