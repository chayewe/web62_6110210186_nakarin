using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Umbrella.Data;
using Umbrella.Models;

using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace Umbrella.Pages.Newmedic
{
    public class CreateModel : PageModel
    {
        private readonly Umbrella.Data.UmbrellaContext _context;

        private IHostingEnvironment _environment;

        public CreateModel(Umbrella.Data.UmbrellaContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        [BindProperty]
        public Medic Medic { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            IFormFile ImageFile = Request.Form.Files[0];

            var file = Path.Combine(_environment.ContentRootPath, "wwwroot/images/img", ImageFile.FileName);
            using (var fileStream = new FileStream(file, FileMode.Create))
            {
                await ImageFile.CopyToAsync(fileStream);
            }
            
            Medic.imageFileName =  Request.Form.Files[0].FileName;

            _context.Newmedic.Add(Medic);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}