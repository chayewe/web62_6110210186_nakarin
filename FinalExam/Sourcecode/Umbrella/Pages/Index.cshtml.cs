﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Umbrella.Data;
using Umbrella.Models;

namespace Umbrella.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Umbrella.Data.UmbrellaContext _context;

        public IndexModel(Umbrella.Data.UmbrellaContext context)
        {
            _context = context;
        }

        public IList<Medic> Medic { get;set; }

        public async Task OnGetAsync()
        {
            Medic = await _context.Newmedic.ToListAsync();
        }
    }
}
