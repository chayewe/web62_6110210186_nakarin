using Umbrella.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Umbrella.Data
{
    public class UmbrellaContext : IdentityDbContext<User>
    {
        public DbSet<Medic> Newmedic { get; set; }
		
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Umbrella.db");			
        }
    }
}